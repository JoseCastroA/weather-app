import React from 'react'
import CityList from './CityList'

export default {
  title: "CityList",
  component: CityList
}

const cities = [
  {city: "Buenos Aires", country: "Argentina"},
  {city: "Bogotá", country: "Colombia"},
  {city: "Madrid", country: "España"},
  {city: "Montevideo", country: "Uruguay"}
]

export const CityListExample = () => <CityList cities={cities}/>
