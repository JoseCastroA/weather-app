import React from 'react'
import Weather from './Weather'

export default {
  title: "Weather",
  component: Weather
}

export const CloudExample = () => <Weather temperature={10} state="cloud"></Weather>
export const SunnyExample = () => <Weather temperature={10} state="sunny"></Weather>
export const RainExample = () => <Weather temperature={10} state="rain"></Weather>
export const FogExample = () => <Weather temperature={10} state="fog"></Weather>
export const CloudyExample = () => <Weather temperature={10} state="cloudy"></Weather>
